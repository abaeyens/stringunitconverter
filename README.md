# stringunitconverter
[![PyPI version](https://badge.fury.io/py/stringunitconverter.svg)](https://badge.fury.io/py/stringunitconverter)
![coverage](https://gitlab.com/abaeyens/stringunitconverter/badges/master/coverage.svg)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
![downloads](https://img.shields.io/pypi/dm/stringunitconverter)

## Goal
Get the multiplication factor
required to convert from one unit to another,
with the units being given as strings.
In short:
make it easy to convert between units.

## Get it
Install with pip using `pip install stringunitconverter`.
For more information,
please see the [PyPI page](https://pypi.org/project/stringunitconverter/).

## Main concepts
- Use strings to specify units
  (e.g. 'kN').
- Allow quasi all mathematical combinations of units
  (e.g. 'N/m^2').
  Math execution order rules should be respected.
- Keep code short (~100 LOC) and unit-independent
  and put all unit-related data in a data file (JSON).
  It should be easy to add units.
- Focus lies on engineering applications.
  Currency conversion etc. is not supported.
- Working concept:
  1. Keep a data file with a multiplier for each individual unit
     to its [SI base unit(s)](
     https://en.wikipedia.org/wiki/SI_base_unit).
     For example: ft (to m): 0.3048, mmHg (to Pa): 0.00750.
  2. Use this datafile to get the multiplier for each given side to base SI.
     This is done by substituting each prefix and unit in the string
     with the appropriate numerical value.
     The equation, containing only mathematical operations and numbers,
     is then evaluated, returning the multiplier.
  3. The multiplier for the conversion follows
     from dividing the two derived multipliers.

## Limits and notes
- Dimensional correctness is not checked.
- Conversions are always multiplicative.
  Therefore, conversions between coordinate systems with a different origin
  are not (yet) supported.
  For example, between °C and K.
  (For this reason, °C and °F are deliberately unsupported; use K.)
  Conversions between Hz and s are therefore also not possible,
  as this would require inverting the value.
- SI style conventions are followed. Some notes:
  - A multiplication is defined using a space ` ` or
    a multiplication dot `·`.
    The asterisk `*` (Python multiplication symbol) is also supported
  - Units not separated by a space or multiplication dot (e.g. 'Nm' for 'N·m')
    are not in line with SI style conventions
    and are not supported by this parser.
- Power superscripts `²` and `³` are supported, for example `mm²`. 


## Supported prefixes and units
Prefixes: y to Y.  
Units:
- SI: g, N, s, Pa, bar, A, K, mol, Hz, J, W, C, V, Ohm, S, F, L, rad
- Imperial: oz, psi, mi, ft, in, mil, mph
- Nautical: (NM, nmi), (kn, kt)
- other: mmHg, gf, (°, deg, degree)
- also supported: %


## Functions and examples
```python
def add_unit(unit: str, multiplier: str)

def add_json(filename: str, add_to_existing=False)

def multiplier(a: str, b: str) -> float)

def get_factor(a: str, unsafe=False) -> float)

def unit_to_factor_string(a: str) -> str
```

For documentation of these functions:
please see their help strings.

Two usage examples:
```python3
>>> import stringunitconverter as suc
>>>
>>> suc.multiplier('N/m^2', 'kN/cm^2')
10.0
>>> suc.multiplier('1/kPa', '1/(N/m^2)')
0.001
```


## Contribution and development
The Git remote repository is hosted on
[GitLab](https://gitlab.com/abaeyens/stringunitconverter).

### Testing
Tests are in the directory `tests`
in the root directory.
To run the tests, navigate to the root directory
and run
`pytest`.

This requires a local copy of the repo.
Note that the tests will be run on
the `stringunitconverter` Python package.
This package may or may not point to
the code in the local copy of the repo,
depending on how the package was installed.


### To get this repo locally
1. Clone the repo.
The directory with the project will be located
in the current working directory of the terminal.
    ```
    $ git clone https://gitlab.com/abaeyens/stringunitconverter.git
    ```
2. Navigate to the created directory.
    ```
    $ cd stringunitconverter
    ```


## Contact
Have a question, suggestion, ... ?
Your feedback on this project is always appreciated!
Send an e-mail to
[mail@arnebaeyens.com](mailto:mail@arnebaeyens.com).

It is also possible to use the [GitLab issues webpage](
  https://gitlab.com/abaeyens/stringunitconverter/-/issues).
